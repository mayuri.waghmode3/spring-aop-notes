package springAop.model;

public class Circle {
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
		//executes twice once during initialization of bean and then target method in main
		System.out.println("circle's setter is called");
		throw (new RuntimeException());
	}
	
	public String setNameAndReturn(String name) {
		this.name = name;
		this.name += " returned";
		System.out.println("circle's setter is called");
		return this.name;
	}
	
	

}
