package springAop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import springAop.service.FactoryService;
import springAop.service.ShapeService;

public class AopMain {
	public static void main(String args[]){
		//@SuppressWarnings("resource")
		//ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		//ShapeService shapeService = context.getBean("shapeService",ShapeService.class);//.class takes care of casting
		FactoryService factoryService = new  FactoryService();
		ShapeService shapeService = (ShapeService) factoryService.getBean("shapeService");//.class takes care of casting
		shapeService.getCircle();
		//shapeService.getCircle().setName("Dummy Name");
	}

}
