package springAop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import springAop.model.Circle;

@Aspect
public class LoggingAspect {
	/**
	 * execute before getName() executes
	 * restricting to Class : springAop.model.Circle
	 */
	@Before("execution(public String springAop.model.Circle.getName())")
	public void LoggingAdvice1() {
		System.out.println("Advice1 run. Get Method called");
	}
	
	/**
	 * One advice applied to different points by using wildcards
	 * applying to all getters
	 * .. - 0/more arguments, * - for 1/more arguments
	 */
	@Before("execution(* get*(..))")
	public void LoggingAdvice2() {
		System.out.println("Advice2 (using wildcard) run. Get Method called");
	}
	
	/**
	 * explaining usage of point cut expression
	 * best practice - mix and match , combine pointcut exp with logical opretions
	 */
	@Before("allGetters() && allCircleMethods()")
	public void LoggingAdvice3() {
		System.out.println("Advice3 (using wildcard) run. Get Method called");
	}
	
	
	// point cut - type expression only once and use across many advice methods
	//using dummy method allGetters
	@Pointcut("execution(* get*(..))")
	public void allGetters() {}
	
	//Apply pointcut for  all the methods within a class 
	//@Pointcut("execution(* * springAop.model.Circle.*(..))")
	@Pointcut("within(springAop.model.Circle)")
	public void allCircleMethods(){}
	
	//class at root package or sub-package(.. - 0/more)- 
	@Pointcut("within(springAop.model..*)")
	public void allSubpackageClassesMethods(){}
	
	//methods that matches the arg listed in pointcut - @Pointcut(args(""))
	
	/**
	 * JoinPoint - gets info of method that triggered it
	 */
	@Before("allCircleMethods()")
	public void LoggingAdvice4(JoinPoint jointPoint) {
		Circle circle = (Circle) jointPoint.getTarget();
		
		System.out.println("Advice4 for "+circle.getName()+" "+jointPoint.toString());
	}
	
	/**
	 * method run for only string arguments
	 */
	@Before("args(String)")
	public void stringArgMethods() {
		System.out.println("Methods that take string args as call");
	}
	@After("args(name)")
	public void NameArgMethodAfter(String name) {
		System.out.println("method that takes name as args for after execution call" + name);
	}
	/**
	 * method run for only string arguments with preprocessing by getting hold of argument
	 */
	@AfterReturning(pointcut = "args(name)", returning = "returnString")
	public void NameArgMethods(String name, String returnString) {
		System.out.println("method that take name as args for after returning call. name = " + name+", returnString = "+returnString);
	}
	//if throw exception doesn't execute then AfterThrowing wont get executed
	@AfterThrowing(pointcut = "args(name)", throwing = "exception")
	public void exceptionAdvice(String name, RuntimeException exception) {
		System.out.println("method that take name as args for exceptionAdvice at name. name = " + name+" , exception ="+exception);
	}
	// 1 pointcut ,multiple exceptions 
	@AfterThrowing(pointcut = "args(name)", throwing = "exception")
	public void exceptionAdvice1(String name, NullPointerException exception) {
		System.out.println("method that take name as args for exceptionAdvice at name. name = " + name+" , exception ="+exception);
	}
	
	//before and after a target method runs using Around advice type
	//1. we need to have atleast 1 arg i.e proceedingJoinPoint
	//2. target method exec happens here proceedingJoinPoint.proceed()
	// we can miss exec of target method
	//Custom annotation Loggable will exec this 
	@Around("@annotation(springAop.aspect.Loggable)")
	public Object myAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		Object obj = null;
		try {
		System.out.println("Around method before");
		obj = proceedingJoinPoint.proceed();
		System.out.println("Around method after");
		}catch(Throwable e) {
			System.out.println("Around method throwing");
		}
		System.out.println("Around method finally");
		return obj;
	}
	
}
