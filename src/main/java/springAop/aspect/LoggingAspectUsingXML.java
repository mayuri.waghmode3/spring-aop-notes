package springAop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;

public class LoggingAspectUsingXML {
	
	public Object myAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		Object obj = null;
		try {
		System.out.println("Around method before");
		obj = proceedingJoinPoint.proceed();
		System.out.println("Around method after");
		}catch(Throwable e) {
			System.out.println("Around method throwing");
		}
		System.out.println("Around method finally");
		return obj;
	}
	
	//Primitive impl of AOP here
	public void primitiveCallLoggingAdvice() {
		System.out.println("LoggingAspectUsingXML.primitiveCallLoggingAdvice");
	}

}
