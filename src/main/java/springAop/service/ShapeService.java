package springAop.service;

import springAop.aspect.Loggable;
import springAop.model.Circle;
import springAop.model.Triangle;

public class ShapeService {

	private Circle circle;
	private Triangle triangle;
	/**
	 * @return the circle
	 */
	//Custom annotation applied
	@Loggable
	public Circle getCircle() {
		System.out.println("*****Call");
		return circle;
	}
	/**
	 * @param circle the circle to set
	 */
	public void setCircle(Circle circle) {
		this.circle = circle;
	}
	/**
	 * @return the triangle
	 */
	public Triangle getTriangle() {
		return triangle;
	}
	/**
	 * @param triangle the triangle to set
	 */
	public void setTriangle(Triangle triangle) {
		this.triangle = triangle;
	}
	
	
	
}
