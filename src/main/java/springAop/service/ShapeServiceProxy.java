package springAop.service;

import springAop.aspect.LoggingAspectUsingXML;
import springAop.model.Circle;
//Use of Proxy classes - Can make additional calls 
public class ShapeServiceProxy extends ShapeService{
	
	public Circle getCircle() {
		//Doing a before call to primitiveCallLoggingAdvice while executing getCircle()
		(new LoggingAspectUsingXML()).primitiveCallLoggingAdvice();
		return super.getCircle();
	}
}
